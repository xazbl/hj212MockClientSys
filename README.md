# 提供基于HJ212协议的污染源在线模拟终端系统

#### 介绍
提供基于HJ212协议的污染源在线模拟终端系统，系统实现了HJ212协议（污染物在线监控监测系统数据传输标准）的模拟设备终端。生态环境监测等相关开发从业者可以在该系统创建指定监控因子的监控设备，指定连接上位机的ip和端口，用于实时数据或历史数据的调试。污染源监控因子的数据生成支持设定数据曲线、异常率等来更贴近真实设备。

⭐️⭐️⭐️⭐️⭐️⭐️我们可以提供数采仪模拟服务⭐️⭐️⭐️⭐️⭐️⭐️️
1.  加入群 QQ: 642256681，技术交流
2.  提供上位机（接收端）ip和端口，需要上报的监控因子编码 联系群管理员配置
#### 使用说明
1.  HJ212 编解码
```java
public class HJ212CodecTest {
    LoggingHandler loggingHandler = new LoggingHandler(LogLevel.WARN);
    public void test() throws InterruptedException {
        HJ212Data parser = new HJ212Data.Parser("QN=20160801085857223;ST=32;CN=2081;PW=123456;MN=010000A8900016F000169DC0;Flag=5;CP=&&DataTime=20160801085857;RestartTime=20160801085624&&");
        EmbeddedChannel client = new EmbeddedChannel (new StringEncoder(),loggingHandler,new HJ212Codec());
        client.writeOutbound(parser);
        EmbeddedChannel server = new EmbeddedChannel (new LineBasedFrameDecoder(1024)
                , new StringDecoder(),loggingHandler,new HJ212Codec());
        ByteBuf o = client.readOutbound();
        while(null!= o){
            ByteBuf byteBuf = Unpooled.copiedBuffer(o);
            server.writeInbound(byteBuf);
            o = client.readOutbound();
        }
        HJ212Data o1 = server.readInbound();
        System.out.println(o1.getData());
        server.finish();
        client.finish();
    }
}
```
2.  污染物处理
```
QN=20230203000000058;ST=27;CN=2031;PW=123456;MN=20230201;Flag=6;PNUM=2;PNO=1;CP=&&DataTime=20230202000000;a0000-Cou=-0.00,a0000-Min=-0.00,a0000-Avg=-0.00,a0000-Max=0.00,a0000-Flag=N;a0001-Cou=33000.00,a0001-Min=100.00,a0001-Avg=100.00,a0001-Max=100.00,a0001-Flag=N;a0002-Cou=66000.00,a0002-Min=-0.00,a0002-Avg=174.60,a0002-Max=200.00,a0002-Flag=N;a0003-Cou=99000.00,a0003-Min=300.00,a0003-Avg=300.00,a0003-Max=300.00,a0003-Flag=N;a0004-Cou=132000.00,a0004-Min=400.00,a0004-Avg=400.00,a0004-Max=400.00,a0004-Flag=N;a0005-Cou=165000.00,a0005-Min=500.00,a0005-Avg=500.00,a0005-Max=500.00,a0005-Flag=N;a0006-Cou=198000.00,a0006-Min=600.00,a0006-Avg=600.00,a0006-Max=600.00,a0006-Flag=N;a0007-Cou=231000.00,a0007-Min=700.00,a0007-Avg=700.00,a0007-Max=700.00,a0007-Flag=N;a0008-Cou=264000.00,a0008-Min=800.00,a0008-Avg=800.00,a0008-Max=800.00,a0008-Flag=N;a0009-Cou=297000.00,a0009-Min=900.00,a0009-Avg=900.00,a0009-Max=900.00,a0009-Flag=N&&
```
CPS字段解析成如下格式
```json
{
	"groups":{
		"a0000":{
			"Cou":"-0.00",
			"Min":"-0.00",
			"Avg":"-0.00",
			"Max":"0.00",
			"Flag":"N"
		},
		"a0001":{
			"Cou":"33000.00",
			"Min":"100.00",
			"Avg":"100.00",
			"Max":"100.00",
			"Flag":"N"
		},
		"a0002":{
			"Cou":"66000.00",
			"Min":"-0.00",
			"Avg":"174.60",
			"Max":"200.00",
			"Flag":"N"
		},
		"a0003":{
			"Cou":"99000.00",
			"Min":"300.00",
			"Avg":"300.00",
			"Max":"300.00",
			"Flag":"N"
		},
		"a0004":{
			"Cou":"132000.00",
			"Min":"400.00",
			"Avg":"400.00",
			"Max":"400.00",
			"Flag":"N"
		},
		"a0005":{
			"Cou":"165000.00",
			"Min":"500.00",
			"Avg":"500.00",
			"Max":"500.00",
			"Flag":"N"
		},
		"a0006":{
			"Cou":"198000.00",
			"Min":"600.00",
			"Avg":"600.00",
			"Max":"600.00",
			"Flag":"N"
		},
		"a0007":{
			"Cou":"231000.00",
			"Min":"700.00",
			"Avg":"700.00",
			"Max":"700.00",
			"Flag":"N"
		},
		"a0008":{
			"Cou":"264000.00",
			"Min":"800.00",
			"Avg":"800.00",
			"Max":"800.00",
			"Flag":"N"
		},
		"a0009":{
			"Cou":"297000.00",
			"Min":"900.00",
			"Avg":"900.00",
			"Max":"900.00",
			"Flag":"N"
		}
	},
	"infos":{
		"DataTime":"20230202000000"
	}
}

```
3.  Netty 客户端(数采仪)
```java
    HJ212TcpClient hj212TcpClient = new HJ212TcpClient("10.237.124.186", 8085);
    HJ212ClientSession hj212ClientSession = new HJ212ClientSession();
    ListenableFuture<Void> connect = hj212TcpClient.connect(hj212ClientSession);
```
4.  Netty 服务端(上位机)
```java
new NettyTcpServer(8085,60,60,10485760,remotingService,scheduler,hj212DataHandler)
```

5.  Spring Boot 支持
``` xml
 <dependency>
            <groupId>cn.hetra.hj212</groupId>
            <artifactId>HJ212BootStarter</artifactId>
 </dependency>
```
``` yml
hetra:
  hj212:
    port: 8085
    reader-idle: 60
    writer-idle: 60
    max-frame-length: 10485760
```
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
5.  更多加入交流群 QQ(642256681)
    ![更多加入交流群](contact.jpg)