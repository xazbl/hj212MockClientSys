package cn.hetra.hj212.client;

import cn.hetra.hj212.core.HJ212Data;
import org.springframework.util.concurrent.ListenableFuture;

import java.io.Closeable;

public interface TcpConnection extends Closeable {

	/**
	 * Send the given message.
	 * @param message the message
	 * @return a ListenableFuture that can be used to determine when and if the
	 * message was successfully sent
	 */
	ListenableFuture<Void> send(HJ212Data message);

	/**
	 * Register a task to invoke after a period of read inactivity.
	 * @param runnable the task to invoke
	 * @param duration the amount of inactive time in milliseconds
	 */
	void onReadInactivity(Runnable runnable, long duration);

	/**
	 * Register a task to invoke after a period of write inactivity.
	 * @param runnable the task to invoke
	 * @param duration the amount of inactive time in milliseconds
	 */
	void onWriteInactivity(Runnable runnable, long duration);

	/**
	 * Close the connection.
	 */
	@Override
	void close();

}