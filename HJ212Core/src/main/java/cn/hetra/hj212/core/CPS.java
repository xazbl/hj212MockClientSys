package cn.hetra.hj212.core;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

/**
 *
 */
public class CPS implements Serializable {
    protected final List<String> items;
    public List<String> getItems() {
        return items;
    }
    private CPS(List<String> items){
        this.items = items;
    }
    @Override
    public String toString(){
        return Joiner.on(';').join(items);
    }
    public static CPS create(String cps){
        return new CPS(Splitter.on(';').omitEmptyStrings().splitToList(cps));
    }
    public interface CPSEntry<T extends CPSEntry>{
        T addEntry(String key,String value);
    }
    private static class KeyValueBuilder<T extends KeyValueBuilder> extends CPS implements CPSEntry<T>{
        public KeyValueBuilder(){
            super(Lists.newArrayList());
        }
        protected  T addItems(String item){
            items.add(item);
            return (T)this;
        }
        @Override
        public T addEntry(String key, String value) {
            return addItems(Joiner.on('=').join(key,value));
        }
    }
    public static class Builder extends KeyValueBuilder<Builder>{
        public Builder addGroupEntry(Consumer<CPSEntry<Builder>> builder){
            KeyValueBuilder keyValueBuilder = new KeyValueBuilder();
            builder.accept(keyValueBuilder);
            return addItems(Joiner.on(',').join(keyValueBuilder.getItems()));
        }
        public CPS build(){
            return CPS.create(Joiner.on(';').join(getItems()));
        }
    }
    public static class CPSGroups{
        final Map <String, Map<String,String>> groups;
        final Map<String,String> infos;
        public CPSGroups(Map <String, Map<String,String>> groups,Map<String,String> infos){
            this.groups = groups;
            this.infos = infos;
        }
        public Map<String, Map<String, String>> getGroups() {
            return groups;
        }
        public Map<String, String> getInfos() {
            return infos;
        }
    }
    public class Parser{
        public CPSGroups parse(){
            return parseGroup();
        }
        public CPSGroups parseGroup(String ... groups){
            HashBasedTable<String, String, String> polIds = HashBasedTable.create();
            Map<String,String> infos = Maps.newLinkedHashMap();
            for(String cpsItems : getItems()){
                if(cpsItems.contains(",")){
                    for(Map.Entry<String, String>entry:Splitter.on(',').withKeyValueSeparator('=')
                            .split(cpsItems).entrySet()){
                        if(Arrays.stream(groups).anyMatch(t->entry.getKey().endsWith(t))){
                            List<String> strings = Splitter.on('-').splitToList(entry.getKey());
                            polIds.put(strings.get(0),strings.get(1),entry.getValue());
                        }
                    }
                }else{
                    Iterator<String> split = Splitter.on('=').split(cpsItems).iterator();
                    String key = split.next();
                    String value = split.next();
                    infos.put(key,value);
                }
            }
            return new CPSGroups(polIds.rowMap(),infos);
        }

    }

}
