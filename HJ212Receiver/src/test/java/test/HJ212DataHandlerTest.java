package test;

import com.alibaba.fastjson2.JSON;
import cn.hetra.hj212.core.HJ212Codec;
import cn.hetra.hj212.core.HJ212Data;
import cn.hetra.hj212.NettyTcpInboundHandler;
import cn.hetra.hj212.service.HJ212DataHandler;
import cn.hetra.hj212.service.RemotingService;
import cn.hetra.hj212.service.dto.CN2011UploadedData;
import cn.hetra.hj212.service.dto.CN2031UploadedData;
import cn.hetra.hj212.service.dto.CN2051UploadedData;
import cn.hetra.hj212.service.dto.CN2061UploadedData;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.embedded.EmbeddedChannel;
import io.netty.handler.codec.LineBasedFrameDecoder;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.concurrent.ScheduledThreadPoolExecutor;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {HJ212DataHandlerTest.HJ212DataHandlerMock.class, RemotingService.class}, properties = {"spring.profiles.active=dev,ruoyi"})
public class HJ212DataHandlerTest {
     public static class HJ212DataHandlerMock implements HJ212DataHandler {
         @Override
         public void handleData(HJ212Data hj212Data) {

         }

         @Override
         public void handleRealData(CN2011UploadedData data) {
             System.out.println("......."+JSON.toJSONString(data));

         }

         @Override
         public void handleMinuteData(CN2051UploadedData data) {
             System.out.println("......."+JSON.toJSONString(data));
         }

         @Override
         public void handleHourData(CN2061UploadedData data) {
             System.out.println("......."+JSON.toJSONString(data));
         }

         @Override
         public void handleDayData(CN2031UploadedData data) {
             System.out.println("......."+JSON.toJSONString(data));

         }

     }

    LoggingHandler loggingHandler = new LoggingHandler(LogLevel.WARN);

     @Autowired
    ObjectProvider<HJ212DataHandler> hj212DataHandler;

     @Autowired
    RemotingService remotingService;

    @Test
    public void test01() throws InterruptedException {
        HJ212Data parser = new HJ212Data.Parser("QN=20160801085857223;ST=32;CN=2081;PW=123456;MN=010000A8900016F000169DC0;Flag=5;CP=&&DataTime=20160801085857;RestartTime=20160801085624&&");
        EmbeddedChannel client = new EmbeddedChannel (new LineBasedFrameDecoder(1024)
                , new StringDecoder() , new StringEncoder(),loggingHandler,new HJ212Codec());
        client.writeOutbound(parser);
        client.writeOutbound(new HJ212Data.Parser("QN=20160801085857223;ST=32;CN=2011;PW=123456;MN=010000A8900016F000169DC0;Flag=5;CP=&&DataTime=20160801085857;w01001-Rtd=7.1,w01001-Flag=N;w01018-SampleTime=20160801070000,01018-Rtd=2.2,w01018-Flag=N,w01018-EFlag=A01&&"));
        EmbeddedChannel channel = new EmbeddedChannel (new LineBasedFrameDecoder(1024)
                , new StringDecoder(),loggingHandler,new HJ212Codec(),new NettyTcpInboundHandler(remotingService, hj212DataHandler));
        ByteBuf o = client.readOutbound();
        while(null!= o){
            ByteBuf byteBuf = Unpooled.copiedBuffer(o);
            channel.writeInbound(byteBuf);
            o = client.readOutbound();
        }
        for(Object i = channel.readOutbound();null!=i;i = channel.readOutbound()){
            System.out.println(i);
        }
        channel.finish();
        client.finish();

    }
}
