package cn.hetra.hj212.service;

import java.util.concurrent.TimeoutException;

public interface ExecutionContext {
   boolean get() throws TimeoutException;

  String getQN();
}
