package cn.hetra.hj212.service.dto;

import java.util.Map;

public class CN2031UploadedData {
    private final String dataTime;
    private final String mn;
    private final Map<String,Map<String,String>> polIds;

    public CN2031UploadedData(String dataTime, String mn, Map<String,Map<String,String>>  polIds){
        this.dataTime =dataTime;
        this.polIds = polIds;
        this.mn = mn;
    }

    public String getMn() {
        return mn;
    }

    public String getDataTime() {
        return dataTime;
    }

    public Map<String, Map<String, String>> getPolIds() {
        return polIds;
    }
}
