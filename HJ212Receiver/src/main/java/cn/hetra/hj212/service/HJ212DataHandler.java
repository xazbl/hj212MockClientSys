package cn.hetra.hj212.service;

import cn.hetra.hj212.core.HJ212Data;
import cn.hetra.hj212.service.dto.CN2011UploadedData;
import cn.hetra.hj212.service.dto.CN2031UploadedData;
import cn.hetra.hj212.service.dto.CN2051UploadedData;
import cn.hetra.hj212.service.dto.CN2061UploadedData;

public interface HJ212DataHandler {

    void handleData(HJ212Data hj212Data);

    void handleRealData(CN2011UploadedData data);

    /**
     * 1、污染物分钟数据标记取值使用如下规则：
     * 如果污水污染物的实时数据在分钟数据上报时间间隔内出现一个异常值，则污染物分钟数据标记为异常，否则污染
     * 物分钟数据标记为正常；
     * 如果烟气污染物的实时数据在分钟数据上报时间间隔内出现 75%以上的正常值，则污染物分钟数据标记为正常，否
     * 则污染物分钟数据标记为在线监控（监测）仪器仪表故障
     * @param data
     */
    void handleMinuteData(CN2051UploadedData data);

    /**
     * 注：污染物小时数据标记取值使用如下规则：
     * 如果污水污染物的分钟数据在一小时内出现一个异常值，则污染物小时数据标记为异常，否则污染物小时数据标记
     * 为正常；
     * 如果烟气污染物的分钟数据在一小时内出现 75%以上的正常值，则污染物小时数据标记为正常，否则污染物小时数
     * 据标记为在线监控（监测）仪器仪表故障
     * @param data
     */
    void handleHourData(CN2061UploadedData data);

    /**
     * 注：污染物日数据标记取值使用如下规则：
     * 如果污水污染物的小时数据在一日内出现一个异常值，则污染物日数据标记为异常，否则污染物日数据标记为正常；
     * 如果烟气污染物的小时数据在一日内出现 75%以上的正常值，则污染物日数据标记为正常，否则污染物日数据标记
     * 为在线监控（监测）仪器仪表故障
     * @param data
     */
    void handleDayData(CN2031UploadedData data);
}
