package cn.hetra.hj212.spring.boot;

import cn.hetra.hj212.service.RemotingService;
import cn.hetra.hj212.service.ServerImpl;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
@ConditionalOnProperty(name = "hetra.hj212.enabled", havingValue = "true",matchIfMissing = true)
public class Hj212ServerAutoConfiguration {
    @Bean
    public RemotingService hj212RemotingService() {
        return new RemotingService();
    }

    @Bean
    @ConfigurationProperties(prefix = "hetra.hj212")
    public ServerImpl hj212Server(Environment env) {
        return new ServerImpl();
    }
}
